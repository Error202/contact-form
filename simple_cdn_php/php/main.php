<?php
/**
 * Created by Error202
 * Date: 02.11.2019
 */

$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

// Количество отправленных файлов
$total = count($_FILES['files']['name']);

$files = [];
for( $i=0 ; $i < $total ; $i++ ) {
    //Получение временного пути к загруженным файлам
    $tmpFilePath = $_FILES['files']['tmp_name'][$i];
    //Загрузка файлов
    if ($tmpFilePath != "") {
        //Загрузка файлов
        $filename = $_FILES['files']['name'][$i];
        move_uploaded_file($tmpFilePath, 'result/' . $filename);
        $files[] = $filename;
    }
}

$files = implode(', ', $files);
$data = <<<DATA
    name: {$name}
    phone: {$phone}
    email: {$email}
    files: {$files}
DATA;

//Сохранение результатов
file_put_contents('result/result.txt', $data);

echo "Complete" . PHP_EOL;
