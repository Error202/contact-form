$(document).ready(function() {
    $("#files").fileinput({
        showPreview: true,
        showUpload: false,
        showRemove: true,
        allowedFileExtensions: ["jpg"],
        language: 'ru',
        maxFileSize: 2048,
        maxFileCount: 5,
        required: true,
        theme: "fas",
        browseOnZoneClick: true,
        showClose: false,
        removeFromPreviewOnError: true,
        dropZoneTitle: 'Перетащите фото или нажмите для выбора',
        dropZoneClickTitle: '',
        //uploadUrl: '#'
    });

    $("#phone").inputmask({"mask": "+9 (999) 999-9999"});
});